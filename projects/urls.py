from projects.views import list_projects, show_project, create_project
from tasks.views import show_tasks
from django.urls import path

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("detail/", show_tasks, name="show_tasks"),
    path("create/", create_project, name="create_project")
]
